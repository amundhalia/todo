<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\User;
use Laravel\Sanctum\Sanctum;

class LoginTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    /**
     * Test if user can login.
     *
     * @return void
    */
    public function test_login()
    {
        $user = User::create([
                                'name' => 'Aditya Mundhalia',
                                'email' => 'aaditya.mundhalia@gmail.com',
                                'password' => bcrypt('secret'),
                            ]);
        $response = $this->post('/api/login', [
            'email' => $user->email,
            'password' => 'secret',
        ]);
        $response->assertStatus(200);
    }


    /**
     * Test if user can logout.
     *
     * @return void
    */
    public function test_logout()
    {
        Sanctum::actingAs(
            User::factory()->create(),
        );

        $response = $this->get('/api/logout');
        $response->assertStatus(200);
    }
}
