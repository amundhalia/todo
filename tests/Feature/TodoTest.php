<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;
use App\Models\Todo;
use App\Models\User;
use Laravel\Sanctum\Sanctum;

class TodoTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    /**
     * Test if user can list todos.
     *
     * @return void
    */
    public function test_todos_can_be_listed()
    {
        Sanctum::actingAs(
            User::factory()->create(),
        );

        $response = $this->get('/api/todos');
        $response->assertOk();
    }

    /**
     * Test if user can create todos.
     *
     * @return void
    */
    public function test_todos_can_be_created()
    {
        Sanctum::actingAs(
            User::factory()->create(),
        );

        $name = $this->faker->name();
        $response = $this->post('/api/todos',[
            'name' => $name,
            'description' => $this->faker->sentence($nbWords = 6, $variableNbWords = true),
        ]);
        $response->assertCreated();
        $this->assertDatabaseHas('todos', ['name' => $name,]);
    }

    /**
     * Test if user can lookup todo.
     *
     * @return void
    */
    public function test_todos_can_be_looked_up()
    {
        $user = User::factory()->create();
        Sanctum::actingAs(
            $user,
        );

        $todo = Todo::factory()->create(['user_id' => $user->id]);
        $response = $this->get('/api/todos/'.$todo->id);
        $response->assertOk()->assertSeeText($todo->name, $escaped = true);
    }

    /**
     * Test if user can update todo.
     *
     * @return void
    */
    public function test_todos_can_be_updated()
    {
        $user = User::factory()->create();
        Sanctum::actingAs(
            $user,
        );

        $todo = Todo::factory()->create(['user_id' => $user->id]);
        $name = $this->faker->sentence($nbWords = 6, $variableNbWords = true);
        $response = $this->put('/api/todos/'.$todo->id,[
            'name' => $name,
        ]);

        $this->assertDatabaseHas('todos', [
            'name' => $name,
        ]);
        $response->assertStatus(202);
    }

    /**
     * Test if user can remove todo.
     *
     * @return void
    */
    public function test_todos_can_be_deleted_up()
    {
        $user = User::factory()->create();
        Sanctum::actingAs(
            $user,
        );

        $todo = Todo::factory()->create(['user_id' => $user->id]);
        $response = $this->delete('/api/todos/'.$todo->id);

        $this->assertDatabaseMissing('todos', $todo->toArray());
        $response->assertOk();
    }
}
