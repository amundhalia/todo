# Todo

# Framework
[Laravel 8](https://laravel.com/docs/8.x)

# Packages
- [Laravel Sanctum](https://laravel.com/docs/8.x/sanctum)

# Docker
For docker I have used docker composed with [laradock](https://laradock.io/). To install follow below:
```bash
# clone laradock repo
git clone https://github.com/Laradock/laradock.git

# navigate into the directory
cd laradock

#copy .env file
cp .env.example .env

#change php version
PHP_VERSION=8.0

# modyfy nginx default file 
nano nginx/sites/default.conf

root /var/www/todo/public;

# start containers (try sudo if fails)
docker-compose up -d mysql phpmyadmin nginx workspace php-fpm

# now navigate into the container (try sudo if fails)
docker-compose exec --user=laradock workspace bash

```
# Tests
All tests are feature tests and can be run like below:
```bash
php artisan test
```

# Seeder
Seeders are located `database\seeders\UserSeeder` Edit the user as desired.
```bash
# To seed the user
php artisan db:seed
```

# API
Postman collection is attached with the project named Todo.postman_collection.json

# Sample Video
Sample video located [here](https://youtu.be/-qjqlhEOZCQ)
