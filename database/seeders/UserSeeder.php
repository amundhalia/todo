<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::updateOrCreate(['email' => 'aaditya.mundhala@gmail.com'],
            [
                'name'              => 'Aditya Mundhalia',
                'email'             => 'aaditya.mundhalia@gmail.com',
                'password'          => bcrypt('aaditya'),
            ]);
    }
}
