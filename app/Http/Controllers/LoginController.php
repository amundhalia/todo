<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    // this method login user and creates a token
    public function login(Request $request)
    {
        $validate = $request->validate([
            'email'    => 'required|string|email',
            'password' => 'required|string'
        ]);

        if (!Auth::attempt($validate)) {
            return response()->json(['message' => 'Credentials not match'], 401);
        }
        else{
            return response()->json([
                'token' => auth()->user()->createToken('API User')->plainTextToken,
                'user'  => auth()->user()->name
            ], 200);
        }
    }

    // this method signs out users by removing tokens
    public function logout()
    {
        auth()->user()->tokens()->delete();
        return response()->json([
            'message' => 'Tokens Revoked'
        ], 200);
    }
}
