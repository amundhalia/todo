<?php

namespace App\Http\Controllers;

use App\Models\Todo;
use Illuminate\Http\Request;
use Throwable;

class TodoController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(auth()->user()->todos, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'name'            => 'required|max:255',
            'description'     => 'required',
        ]);
        $validated['user_id'] = auth()->user()->id;
        try {
            $todo = Todo::create($validated);
            return response()->json($todo, 201);
        } catch (Throwable $e) {
            return response()->json($e, 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Todo  $todo
     * @return \Illuminate\Http\Response
     */
    public function show(Todo $todo)
    {
        if($todo->user_id == auth()->user()->id){
            return response()->json($todo, 200);
        }
        return response()->json(['message' => 'unauthorized'], 401);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Todo  $todo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Todo $todo)
    {
        $validatedData = $request->validate([
            'name'            => 'required|max:255',
            'description'     => '',
        ]);

        if($todo->user_id == auth()->user()->id){
            $todo = $todo->update($validatedData);
            if(!$todo){
                return response()->json(['message' => 'unable to process'], 406);
            }
            return response()->json(['message' => 'entry updated successfully'], 202);
        }
        return response()->json(['message' => 'unauthorized'], 401);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Todo  $todo
     * @return \Illuminate\Http\Response
     */
    public function destroy(Todo $todo)
    {
        if($todo->user_id == auth()->user()->id){
            $todo->delete();
            return response()->json(['message' => 'entry removed successfully'], 200);
        }
        return response()->json(['message' => 'unauthorized'], 401);
    }
}
